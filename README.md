=== MyBookTable ===
Contributors: authormedia, neovita
Donate link: http://www.authormedia.com/
Tags: podcast, Subscribe, iTunes, Sticher Radio, Miro 
Requires at least: 3.9.0
Tested up to: 3.9.0
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A WordPress Bookstore Plugin to help authors create great looking book pages that rank well on Google.

== Description ==

Add This Podcast makes it easy for your listeners to subscribe to your podcast in their platfrom of choice.


= FEATURES =

= One Click Subscribe Buttons =
Allow your users to unscribe to your podcast in one click on their favorite podcast platfrom.

= Podcast Subscribe Widget =
Easily add a subscribe box widget to your website.

= Podcast Subscribe Shortcode =
Easily add a subscribe box via a simeple shortcode.

= Support for the Following Podcast Directories =
* iTunes
* Sticher Radio
* Miro


= One Click Subscribe Buttons =
MyBookTable comes with Buy Buttons for the following stores:

* Downcast
* 

= Premium Buy Buttons =
The following buy buttons are available with the Pro & Dev Add-On for MyBookTable.


== Installation ==

= Old Fashioned FTP Method =
1. Download the mybooktable.zip from the WordPress plugin repository.
2. Open the zip to a directory named mybooktbale. This should happen automatically by double clicking the zip file.
3. Upload the mybooktable directory to the /wp-content/plugins/ directory of your site, using your favorite FTP client.
4. Click “Activate” under the the plugin on the 'Plugins' menu in WordPress.

= Zip Upload Method =
1. Download the mybooktable.zip from the WordPress plugin repository.
2. In your WordPress Dashboard go to Plugins -> Add New -> Upload
3. Select choose file, select mybooktable.zip and click “install now”
4. Click “Activate” under the the plugin on the 'Plugins' menu in WordPress.

= Repository Crossload Method (Recommended) =
1. In your WordPress Dashboard go to Plugins -> Add New -> Search
2. Search for MyBookTable then click install
3. Click “Install”
4. Click “Activate” under the the plugin on the 'Plugins' menu in WordPress.


== Screenshots ==
1. No Screenshots Yet.

== Upgrade Notice ==

= 0.1.0 =
This is the initial version.


== Changelog ==

= 0.1.0 = Initial Version
