<?php
/*
Plugin Name: Add This Podcast
Plugin URI: http://www.castlemediagroup.com/
Description: Allows you to add a widget with Podcast Subscription buttons
Version: 0.85
Author: Jim Camomile - Castle Media Group
Author URI:http://www.castlemediagroup.com/
License: GPL2
*/
require('includes/atp_buttonsorter.php'); // arrange buttons
require('includes/atp_integrations.php'); // the widget
require('includes/atp_widget.php'); // the widget
require('includes/atp_shortcode.php'); // the shortcode
require('includes/atp_defaultpage.php'); // creates the default page and adds a QR code
require('includes/atp_admin.php'); // the admin panel



function atp_services() {
// Warning, only add new services to the end
	$services = array(
		'itunes',
		'Miro',
		'Spreaker',
		'RSS_Feed',
		'Feedburner_Email',
		'Stitcher',
	//	'Zune',
		'Downcast',
		'Pocketcasts',
	);
	$nodes = array();
	$i=1;
	$order = get_option('atp-sort-podcasts');
	foreach ($services as $serv){
		$sort = $order[$i];
		$nodes[$serv]['id'] = $i;
		$nodes[$serv]['sort'] = $sort;
		$nodes[$serv]['name'] = strtolower($serv);
		$nodes[$serv]['url'] = strtolower($serv).'_url';
		$nodes[$serv]['title'] = str_replace('_',' ',$serv);
		$nodes[$serv]['bbutn'] = $serv.'_bigimg.png';
		$nodes[$serv]['smbutn'] = $serv.'_smimg.png';
		$nodes[$serv]['cbutn'] = $serv.'_cbutton.png';
		$i++;
	}
	return $nodes;
}


function get_ordered_services(){
	$services = atp_services();
	foreach ($services as $key => $serv){
		if( empty($serv['sort']) ){
		//	$services[$key]['sort'] = $serv['id']; // not sure I want to make ids and sorts match by default
		}
	}
	usort($services, 'sortByOrder');
	return $services;
}

function sortByOrder($a, $b) {
    return $a['sort'] - $b['sort'];
}

// output the services
function my_podcast_services($args=array()){

	// get the services
	$services = get_ordered_services(); 
	// Get the options
	$servlinks = get_option('cap_add-this-podcast');
	$linktype = $servlinks['button_type'];
	$shortcode = ( isset($args['excludes']) ? true : false);

	// if called from shortcode, unpack the excluded services into an array. also set the heading for the block
	
	if ( $shortcode ) {
		$excludes = array();
		if ( !empty($args['excludes']) ){
			$excludes = explode(',',$args['excludes']);
			$excludes = array_filter(array_map('trim', $excludes)); // removes any whitespace in shortcode comma sep entries
		}
	
		foreach ($services as $serv ){
			if( !in_array( $serv['name'],$excludes )){
				$args[$serv['name'].'_use'] = 'on';
			}
		}
	} // end if isset excludes
	
	$heading = $args['title'];
	
	// if called by the widget, then unpack the instance array
	if (!empty($args)){
		$linktype = $args['linktype'];
	}
	// base images url
	$siteurl = get_option('siteurl');
	$url = $siteurl . '/wp-content/plugins/' . basename(dirname(__FILE__)) . '/images/';
	
	
	$output = '<div class="atp-link-wrapper '.$args['class'].'">';
	if(isset($heading) && !empty($heading) && $shortcode ){
		$output .= '<h3>'.$heading.'</h3>';
	}
	$output .= '<ul class="'.$linktype.'">';
	
	foreach($services as $serv){
		$servname = $serv['name'];
		$servtitle = str_replace('_',' ',$servname);
		if($servname != 'itunes'){  // yeah itunes does not want its name to be capitalized "dare to be different"
			$servtitle = ucwords($servtitle);
		}
		// test to see if widget or shortcode overrides display of each button
		
		if (!empty($args) && ( isset($args[$servname.'_use']) && $args[$servname.'_use'] == 'on' ) ){
			$linkval = $servlinks[$servname.'_link']; 
			$serv['bbutn'] = $url.$servname.'-button.png';
			$serv['smbutn'] = $url.$servname.'-sbutton.png';
			$serv['cbutn'] = $url.$servname.'-cbutton.png';
			if ($linktype == 'bbutn'){
				$link_obj = '<img class="big" src="'.$serv['bbutn'].'" />';
			} elseif ($linktype == 'sbutn'){
				$link_obj = '<img class="small" src="'.$serv['smbutn'].'" />';
			} elseif ($linktype == 'cbutn'){
				$link_obj = '<img class="classic" src="'.$serv['cbutn'].'" />';
			} else {
				$link_obj = $serv['title'];
			}
		
			if(isset($linkval) && !empty($linkval) ){
				$output .= '<li class="link-box '.$serv['name'].'">
						<a href="'.$linkval.'" class="'.$linktype.' btn" target="_blank" title="subscribe with '.$servtitle.'" alt="subscribe with '.$servtitle.'">
							'.$link_obj.'
						</a>
					</li>';
			} // end if
		} // end if

	} // end foreach
	
	$output .= '</ul>';
	$output .= '</div>';
	// end after text
	return $output;
}

// Widget CSS
function atp_css_load() {
	$dir = plugins_url(basename(dirname(__FILE__)));
	$url = $dir . '/atp-styles.css';
	wp_register_style( 'atp-styles', $url);
	wp_enqueue_style('atp-styles');
}
add_action( 'wp_enqueue_scripts', 'atp_css_load', 20 );



// CSS for Admin
if ( ! function_exists( 'atp_admin_css_load' ) ) {
	function atp_admin_css_load() {
		$siteurl = get_option('siteurl');
		$url = $siteurl . '/wp-content/plugins/' . basename(dirname(__FILE__)) . '/atp_admin.css';
		wp_register_style( 'atp-admin', $url);
		wp_enqueue_style( 'atp-admin');
	}
}
add_action( 'admin_head', 'atp_admin_css_load', 20 );

?>