<?php
// Adds a Shortcode for AddMyPodcast
// Vanilla shortcode 
function atp_show_podcast_links($atts) {
	$options = get_option('cap_add-this-podcast');
	$args = shortcode_atts( array(
		'title' => $options['above_buttons'],
		'excludes' => '',
		'linktype' => $options['button_type']
	), $atts ) ;
	
	$output = '<div id="atp-links">'; 
	$output.= my_podcast_services($args);
	$output.='</div>';
	
	return $output;
}
// Make this a shortcode
add_shortcode( 'atp_links', 'atp_show_podcast_links' ); 


/********************************************/
/*    Thickbox pop-up					    */
/*                                          */
/********************************************/

function atp_show_podcast_links_popup($atts){
	$options = get_option('cap_add-this-podcast');
	extract( shortcode_atts( array(
		'title' => $options['above_buttons'],
		'excludes' => '',
		'linktype' => $options['button_type'],
		'triggerID' => ''
	), $atts ) );
	
	$output = '<div id="atp-links">'; 
	$output.= my_podcast_services($atts);
	$output.='</div>';
	$triggerID = ( isset($atts['triggerID']) && !empty($atts['triggerID']) ? $atts['triggerID'] : "TB_inline");
	return atp_load_thickbox_div($output,$triggerID);
}
// Make this a shortcode
add_shortcode( 'atp_links_popup', 'atp_show_podcast_links_popup' ); 

// Popup Function utilizing add_thickbox.
function atp_load_thickbox_div($content,$triggerID="TB_inline"){
	add_thickbox();
	$divID='atp-buttonsbox';
	echo '<div id="'.$divID.'" style="display:none;">
		 <p>';
	echo $content;	  
	echo '</p>
	</div>';
	//echo '<a href="#'.$triggerID.'?width=300&height=400&inlineId='.$divID.'" class="thickbox button">Subscribe!</a>';
}

// add the popup params to the correct id with jquery
function set_popup_params_inbutton($triggerID,$divID){
	echo '
	<script type="text/javascript">
		var params = { width:300, height:400, inlineID:"'.$divID.'" };
		var str = jQuery.param( params );
		jQuery( "#'.$triggerID.'" ).text( str );
	</script>';
}

function set_popup_jquery(){
	if (!is_front_page()){
	//	return;
	}
	$triggerID = 'podcast-itunes-link';
	$divID = 'atp-buttonsbox';
	echo set_popup_params_inbutton($triggerID,$divID);
}

//add_action('wp_head','set_popup_jquery',4000);

// vanilla shortcode options
/* default
[atp_links  title="Add This Podcast!" excludes="miro,itunes" linktype="bbutn"]

parameters:

title="" 
This is the line that goes about the podcast links. It can be empty or anything you you like. 

Defaults to the title set in the main plugin options

excludes="" 
This is a comma separated list of the services you want to exclude from displaying. Possible enties include:
itunes
miro
rss_feed
feedburner_email
stitcher
downcast

Defaults to none. If a service does not have a link inputted in the plugin admin panel, then it will not display

linktype=""
Defines the type of buttons or links you want to display. Possible entries include:

bbutn (Big Buttons)
sbutn (small or square buttons)
text  (text links)

Defaults to the setting in the plugin admin panel

Extra parameter for the popup shortcode - triggerID, names the id of the button, a link that triggers the popup

[atp_links_popup  title="Add This Podcast!" excludes="miro,itunes" linktype="bbutn" triggerID="mydiv" ]
*/