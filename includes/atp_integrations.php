<?php
//  Integration Helper Functions

// Powerpress integration
function blubrry_feed_info(){
	$blubrry_gen = get_option('powerpress_general');
	$blubrry_feed = get_option('powerpress_feed');
	$links = get_option( 'atp_powerpress_feeds' ) ? get_option( 'atp_powerpress_feeds' ) : '';
	$info = array();
	
	$info['title'] = $blubrry_feed['title'];
	$info['subtitle'] = $blubrry_feed['itunes_subtitle'];
	$info['summary'] = $blubrry_feed['itunes_summary'];
	$info['description'] = $blubrry_feed['description'];
	$info['itunes_url'] = $blubrry_feed['itunes_url']; 
	// I am not sure yet, what we need to do about multiple feeds, so we will just return the first in the array
	$info['feed_links'] = $links[0];
	
	return $info;
}
add_action( 'admin_head', 'blubrry_feed_info', 20 );

// get the powerpress info and save it in an option
function powerpress_feed_links()
{
	// PowerPress settings:
	$ppress = get_option('powerpress_general');
	if( !isset($ppress['custom_feeds']) )
    $ppress['custom_feeds'] = array('podcast'=>'Default Podcast Feed');
	if( !empty($ppress['feed_links']) )
	{	
		$myfeeds = array();
		// Loop through podcast feeds and add each to the array
		while( list($feed_slug, $title) = each($ppress['custom_feeds']) )
		{
			$href = get_feed_link($feed_slug);
			if ( isset($title) && isset($href) ){
				$myfeeds[] = $href;
			}
		}
	}
	add_option( 'atp_powerpress_feeds', $myfeeds, '', 'yes' );
	return ;
}
add_action('admin_init','powerpress_feed_links');


/******************************************/
/*             Service Filters            */
/*       filter in customized links       (/
/*****************************************/

// rss link - from powerpress
function rss_feed_default_link($url){
	if(isset($url) && !empty($url) ){
		return $url;
	}
	$info = blubrry_feed_info();
	
	// I still do not know what to do if there are more than one feeds so using the first one which should be default
	$rss_url = $info['feed_links']; 
	return $rss_url;
}
add_filter('rss_feed_link','rss_feed_default_link');


// rss link - from powerpress
function feedburner_email_default_link($url){
	if(isset($url) && !empty($url) ){
		return $url;
	}
	$info = blubrry_feed_info();
	$rss_url = $info['feed_links']; 
	// check to see if this is feedburner. We will probably find other similar services for the base podcast feed later
	if(strpos($rss_url, 'feedburner')){
		// get the name after the final /
		$name = substr( $rss_url, strrpos( $rss_url, '/' )+1 );
		$email_url = 'http://feedburner.google.com/fb/a/mailverify?uri='.$name;
		return $email_url;
	}
	return $url;
	// http://feeds.feedburner.com/NovelMarketing
	// http://feedburner.google.com/fb/a/mailverify?uri=tmcafeblog
	// https://itunes.apple.com/us/podcast/novel-marketing/id721122555?mt=2
}
add_filter('feedburner_email_link','feedburner_email_default_link');


// itunes link - from powerpress
function itunes_default_link($url){
	if(isset($url) && !empty($url) ){
		return $url;
	}
	$info = blubrry_feed_info();
	$itunes_url = $info['itunes_url'];
	return $itunes_url;
}
add_filter('itunes_link','itunes_default_link');

/* Downcast link */
function downcast_default_link($url){
	if(isset($url) && !empty($url) ){
		return $url;
	}
	$info = blubrry_feed_info();
	$rss_url = $info['feed_links']; 
	// check to see if this is feedburner
	if(strpos($rss_url, 'feedburner')){
		// get the name after the final /
		$name = substr( $rss_url, strrpos( $rss_url, '/' )+1 );
		$email_url = 'downcast://feeds.feedburner.com/'.$name;
		return $email_url;
	}
	return $url;
}
add_filter('downcast_link','downcast_default_link');

// filters for specifics for various services

/******************************************/
/*******  Admin Desctiptions  *************/
/******************************************/

// itunes description 
function atp_description_for_itunes($content){
	$content = 'i.e. https://itunes.apple.com/us/podcast/novel-marketing/id721122555?mt=2 <br /> If you are using Powerpress and have added an itunes account, it will pre-fill here. <br /> <a href="https://www.apple.com/itunes/podcasts/specs.html/" target="_blank" >Set up an account at iTunes and create a link </a>';
	return $content;
}          
add_filter('atp_itunes_description','atp_description_for_itunes');

// miro description 
function atp_description_for_miro($content){
	$content = 'i.e. http://www.miroguide.com/feeds/18044 . This requires an account at Miro. </br /> <a href="http://www.getmiro.com/publish/guide/" target="_blank" >Set up an account at Miro and create a link </a>';
	return $content;
}          
add_filter('atp_miro_description','atp_description_for_miro');

// spreaker description 
function atp_description_for_spreaker($content){
	$content = 'i.e. http://www.spreaker.com/user/7265145 . This requires an account at Spreaker. <br /> <a href="http://www.spreaker.com/signup/" target="_blank" >Set up an account at Spreaker and create a link </a>';
	return $content;
}          
add_filter('atp_spreaker_description','atp_description_for_spreaker');

// RSS description 
function atp_description_for_rss_feed($content){
	$content = 'i.e. '.site_url().'/feed or, if your podcast is single category in your blog, '.site_url().'podcastcategory/feed , or if you are using feedburner, something like this: http://feeds.feedburner.com/NovelMarketing. If you are using powerpress, this will prefill for you.' ;
	return $content;
}          
add_filter('atp_rss_feed_description','atp_description_for_rss_feed');

// Feedburner description 
function atp_description_for_feedburner_email($content){
	$content = 'i.e. http://feeds.feedburner.com/NovelMarketing. If you are using powerpress, this will prefill for you.';
	return $content;
}          
add_filter('atp_feedburner_email_description','atp_description_for_feedburner_email');

// Stitcher description 
function atp_description_for_stitcher($content){
	$content = 'i.e. http://www.stitcher.com/podcast/novel-marketing . This requires an account at Stitcher. <br /> <a href="http://www.stitcher.com/content-providers/" target="_blank" >Set up an account at Stitcher and create a link </a>';
	return $content;
}          
add_filter('atp_stitcher_description','atp_description_for_stitcher');

// Downcast description 
function atp_description_for_downcast($content){
	$content = 'i.e. downcast://feeds.feedburner.com/NovelMarketing <br /> If you are using Powerpress and have added an rss feed, this will pre-fill here';
	return $content;
}          
add_filter('atp_downcast_description','atp_description_for_downcast');

// Pocketcasts description 
function atp_description_for_pocketcasts($content){
	$content = 'i.e. http://pcasts.in/feed/feeds.feedburner.com/NovelMarketing <br /> If you are using Powerpress and have added an rss feed, this will pre-fill here';
	return $content;
}          
add_filter('atp_pocketcasts_description','atp_description_for_pocketcasts');

/// END DESCRIPTIONS

/* Pocketcasts link */
function pocketcasts_default_link($url){
	if(isset($url) && !empty($url) ){
		return $url;
	}
	$info = blubrry_feed_info();
	$rss_url = $info['feed_links']; 
	// check to see if this is feedburner
	if(strpos($rss_url, 'feedburner')){
		// get the name after the final /
		$name = substr( $rss_url, strrpos( $rss_url, '/' )+1 );
		$email_url = 'http://pcasts.in/feed/feeds.feedburner.com/'.$name;
		return $email_url;
	}
	return $url;
}
add_filter('pocketcasts_link','pocketcasts_default_link');


// show buttons under Powerpress podcast episodes
function buttons_under_powerpress_posts($content){
	$options = get_option('cap_add-this-podcast');
	// return if powerpress not loaded or option not set
	if ( !function_exists('powerpress_content') || ( !isset($options['show_buttons_in_posts']) || $options['show_buttons_in_posts'] != 'on' ) ){
		return $content;
	}
	
	// return if post has no "episode" postmeta row
	$enclosure = get_post_meta(get_the_ID(), 'enclosure');
	if ( !isset($enclosure) || empty($enclosure) ){
		return $content;
	}
	
	// load the function in a div at end of content
		$args = array(
			'title' => 'Subscribe',
			'excludes' => '',
			'linktype' => 'sbutn',
			'class' => 'powerpress'
		);

	$links = my_podcast_services($args);
	return $content.$links;
} 
add_filter('the_content','buttons_under_powerpress_posts');