<?php
if(!class_exists('Add_This_Podcast_Widget')) {
                  
	class Add_This_Podcast_Widget extends WP_Widget {
				
		function __construct() {
			$widget_ops = array('classname' => 'add_this_podcast', 'description' => __('Adds a group of podcast subscription buttons.'));
			$control_ops = array('width' => 400, 'height' => 350);
			parent::__construct(false, 'Add This Podcast Widget', $widget_ops, $control_ops);
		}

		function widget($args, $instance) {	
			
			/* Provide some defaults */
			$defaults = array( 'title' => 'Subscribe to my Podcast', 'text_before_form' => '', 'text_after_form' => '');
			$instance = wp_parse_args( (array) $instance, $defaults );	
			
			extract( $args );
			//extract($instance);
			
			$title = apply_filters('widget_title', $instance['title']);
			$linktype = $instance['linktype'];
			
			echo $before_widget;
			
			// insert the title
			 echo '<h3>'.$title.'</h3>'; 
			
			// output the services
			echo my_podcast_services($instance);
						
			echo $after_widget; 
		}

		function update($new_instance, $old_instance) {
			$services = atp_services();
			$instance = array();
			$instance['title'] = strip_tags( $new_instance['title'] );
			$instance['linktype'] = strip_tags( $new_instance['linktype'] );
			foreach ($services as $serv){
				$instance[$serv['name'].'_use'] =  $new_instance[$serv['name'].'_use'];
			}
			return $instance;			
		}

		function form($instance) {	
			global $woo_options;
			// To Do - feed the services from the options
			$services = atp_services();
			$options = get_option('cap_add-this-podcast');
			$defaults = array( 'title' => 'Podcast Links', 'text_before_form' => '', 'text_after_form' => '');
			$instance = wp_parse_args( (array) $instance, $defaults );		
			extract($instance);
			$title = strip_tags($title);

			?>

  			<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $title ); ?>"   id="<?php echo $this->get_field_id('title'); ?>" />
			</p>
			
			 <p>
			<label for="<?php echo $this->get_field_id('linktype'); ?>">Link Type (overrides default settings): </label>
            <select name="<?php echo $this->get_field_name( 'linktype' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'linktype' ); ?>">
                <option value="bbutn" <?php selected( $instance['linktype'], 'bbutn' ); ?>><?php _e( 'Wide Buttons', 'woothemes' ); ?></option>
                <option value="sbutn" <?php selected( $instance['linktype'], 'sbutn' ); ?>><?php _e( 'Square Buttons', 'woothemes' ); ?></option>
				 <option value="cbutn" <?php selected( $instance['linktype'], 'cbutn' ); ?>><?php _e( 'Classic Buttons', 'woothemes' ); ?></option>
                <option value="text" <?php selected( $instance['linktype'], 'text' ); ?>><?php _e( 'Text Links', 'woothemes' ); ?></option>
            </select>		
			</p>

			<p>
        	
        	<label for="<?php echo $this->get_field_id( 'social' ); ?>"><?php _e( 'Choose the Services to Display', 'woothemes' ); ?></label>
	   		</p>
		
			<?php
			foreach ($services as $serv){
			$active = ( !empty($options) && ( isset($options[$serv['name'].'_link']) && !empty($options[$serv['name'].'_link']) )) ? true : false;
			if (!$active){
				$style = 'disabled';
				$status = 'disabled="disabled"';
				$statusmessage = '<a href="/wp-admin/admin.php?page=add-this-podcast" target="_blank"> Enable this at Settings Page </a>';		
			} else {
				$style = 'enabled';
				$status = '';
				$statusmessage = '';		
			}	
			$foruse = $instance[$serv['name'].'_use'];
			
			?>
				<div class="box <?php echo $serv['name']; ?> <?php echo $style; ?>">			
					<p>
						<input id="<?php echo $this->get_field_name($serv['name'].'_use'); ?>" name="<?php echo $this->get_field_name($serv['name'].'_use'); ?>" type="checkbox"<?php if($active){checked( $foruse, 'on' );} ?> <?php echo $status; ?> />
						<label for="<?php echo $this->get_field_id($serv['name'].'_use'); ?>"> Show <?php echo $serv['title']; ?><span style="margin-left: 10px;"> <?php echo $statusmessage; ?> </label></span>
					</p>	
				</div> <!-- end <?php echo $serv['title']; ?> -->
							
			<?php } // end foreach 	?>
			<p style="margin: 10px; padding: 10px; clear: both; border: 1px solid #AEAEAE;"> <a href="/wp-admin/admin.php?page=add-this-podcast" target="_blank"> Go to Settings Page for Additional settings </a>	</p>	

			<?php 
		}
			
	} 
}
add_action( 'widgets_init', create_function( '', 'return register_widget("Add_This_Podcast_Widget");' ), 1 ); 