<?php
//Include the code
require_once('lib/custom-admin-pages.php');

$services = atp_services();

$servarray = array();
$cnt = 0;
foreach ($services as $serv){ 
	$defval = apply_filters($serv['name'].'_link', '');
	$servarray[$cnt]['name'] = $serv['title'];
	$servarray[$cnt]['desc'] = apply_filters('atp_'.$serv['name'].'_description','');
	$servarray[$cnt]['id'] = $serv['name'].'_link';
	$servarray[$cnt]['type'] = 'text';
	$servarray[$cnt]['default'] = $defval;
	
	$cnt++;
} 

function servicenames(){
	global $services;
	$output = '<div class="atp-service-list">';
	foreach ($services as $key => $sname){ 
		$output .= $services[$key]['name'].'<br />';
	}
	$output .= '</div>';
	return $output;
}

	
cap_add_menu_page("Add This Podcast", "add-this-podcast", 
	array(
		array(
			'title' => 'General Options',
			'desc' => '',
			'id' => 'general_options',
			'settings_fields' => array(
				/*array(
					'name' => 'Import Settings from Powerpress',
					'desc' => 'Check this only if want to import settings from Powerpress',
					'id'   => 'import_powerpress',
					'type' => 'checkbox',
				),*/
				array(
					'name' => 'Podcast Bar Heading',
					'desc' => 'Optional - the text that appears above the Buttons ',
					'id'   => 'above_buttons',
					'type' => 'text',
				),
				/*array(
					'name' => 'Text Below Button Bar',
					'desc' => 'Optional - the text that appears below the Buttons',
					'id'   => 'below_buttons',
					'type' => 'text',
				),*/

			)
		),
		array(
			'title' => 'Powerpress Options',
			'desc' => 'Only for use if you are using the Blubrry PowerPress Plugin for your podcasts',
			'id' => 'powerpress_options',
			'settings_fields' => array(
				array(
					'name' => 'Show Buttons under each podcast',
					'desc' => 'Optional - If checked, your subscribe buttons will show under the media player in each podcast post. ',
					'id'   => 'show_buttons_in_posts',
					'type' => 'checkbox',
				),
			)
		),
		array(
			'title' => 'Default Button Type',
			'desc' => 'Choose the default style of buttons you would like to user',
			'id' => 'button_options',
			'settings_fields' => array(
				array(
					'name' => 'Button Type',
					'desc' => '',
					'id'   => 'button_type',
					'type'    => 'radio',
							'options' => array(
							'bbutn' => __( 'Wide Buttons', 'cmb' ),
							'sbutn' => __( 'Square Buttons', 'cmb' ),
							'cbutn' => __( 'Classic Buttons', 'cmb' ),
							'text' => __( 'Text Links', 'cmb' )
						),
				),
			)
		),
		array(
			'title' => 'Services',
			'desc' => 'Add the url for each service you want to use',
			'id' => 'service_options',
			'settings_fields' => $servarray,
		),
		
		array(
			'title' => 'Create a Default AddThisPodcast Page',
			'desc' =>  create_default_page_message(),
			'id' => 'create_default_page',
			'settings_fields' => array(),
		),

		
		array(
			'title' => 'Usage Instructions',
			'desc' => '
			<table class="form-table instructions">
				<tbody>
					<tr>
						<th scope="row">Add This Podcast Widget</th>
						<td>
							The widget has additional settings for choosing which of the services to display in the widget. Settings chosen in the widget settings will override those set on this settings page. Services with no url set on this settings page(above) will appear as inactive in the widget settings.  
						</td>
					</tr>
					<tr>
						<th scope="row">Shortcodes</th>
						<td>
							<ul style="margin-top: 20px;">
								<li> Adding the basic shortcode, <strong>[atp_links]</strong> will display all the active service buttons with the button type chosen on this settings page (above) </li>
								<li> For more advanced usage, there are 3 additional shortcode parameters that you can set.</li>
								<ul>
									<li>Show a heading above shortcode buttons: <strong>[atp_links title="Add Your Title"]</strong> .</li>
									<li>Exclude some active services (comma separated list): <strong>[atp_links excludes="miro,spreaker"]</strong> . <br />Possible Choices: <br />'.servicenames().'</li>
									<li>Change the Button Type from the default: <strong>[atp_links linktype="sbutn"]</strong>. <br />Possible Choices: 
									
									<div class="button-choices"> 
										<ul>
											<li> bbutn  (Shows Large, Wide Buttons)</li>
											<li> sbutn (Shows Square Buttons)</li>
											<li> cbutn (shows small Classic Buttons)</li>
											<li> text (Show Text Links)</li>
										</ul> 
									</div>
									</li>
								</ul>
							</ul>
						</td>
					</tr>
					<tr>						
						<th scope="row">Change Button Order</th>
						<td>
						Currently there is one place to set the button order for widgets and shortcodes. <a href="/wp-admin/admin.php?page=atp-sort-buttons">Go Here</a> to drag and drop the preferred order and then hit "Save Changes" The button sorter will show all supported services whether or not they are active.
						</td>
				</tbody>
			</table>
			
			
			
			',
			'id' => 'use_instructions',
			'settings_fields' => array(),
		),		
		
	)
);


//$stuff = get_option('cap_add-this-podcast');
?>