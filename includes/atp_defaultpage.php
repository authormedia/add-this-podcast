<?php
// creates a default page with the basic shortcode when url var createatppage=true is passed
function atp_create_default_page() {
	$atp_page = get_option('atp-defaultpage');
	// check to see if the page is already there - looks in cap_add-this-podcast for option with postID of default page
	if ( isset($atp_page) && !empty($atp_page) ){
		return;
	}
	
	// nonce? 
	
	// set page defaults
	$post = array(
	  'post_content'   => ' [atp_links] ',
	  'post_name'      => 'add-this-podcast',
	  'post_title'     => 'Subscribe to my Podcast',
	  'post_status'    => 'publish',
	  'post_type'      => 'page'
	);  
	
	// create the page
	$defaultpage = wp_insert_post( $post, true);
	
	// make postmeta with atp_default_page = true
	add_post_meta($defaultpage, 'atp_default_page', 'true', true);
	// set the option for the page id
	$atp_page = $defaultpage;
	update_option( 'atp-defaultpage', $atp_page );
	
	// add the qr code
	
	return ;
}

if (isset($_GET['createatppage']) && $_GET['createatppage'] == true){
	add_action('admin_init','atp_create_default_page');
}

// removes default atp page option if that page is deleted
function unset_default_page($postID){
	$defpage = get_post_meta($postID, 'atp_default_page', true);
	if( isset($defpage) && !empty($defpage) ){
		delete_option('atp-defaultpage');
		print_r('<pre style="padding: 10px; border: 1px solid #000; margin: 10px">'); print_r( 'removed the option' ); print_r('</pre>'); exit;
	}
}
add_action( 'before_delete_post', 'unset_default_page' );

// displays appropriate message on settings page
function create_default_page_message(){
	$atp_page = get_option('atp-defaultpage');
	//print_r('<pre style="padding: 10px; border: 1px solid #000; margin: 10px">'); print_r( $atp_options ); print_r('</pre>');
	$nopageyet = 'Click the button below to create a default page displaying your AddThisPodcast Buttons. It will also display a QR code you can use in print media.
	<div class="button-box"><a href="/wp-admin/admin.php?page=add-this-podcast&createatppage=true">Create Page</a></div>
	';
	if( get_post_status( $atp_page ) == 'trash' ){
		$intrash = ', but it is in the trash';
		$adminlink = '/wp-admin/edit.php?post_status=trash&post_type=page';
	} else {
		$intrash = '';
		$adminlink = '/wp-admin/post.php?post='.$atp_page.'&action=edit';
		$pagelink = site_url() . '/?p='.$atp_page.'/';
	}
	$isapage = 'You already have a <a href="'.$adminlink.'" />default page</a>'.$intrash.'. Click the link to edit or remove this page.';
	// QR Code
	$isapage .= '
	<div>
		<h3>QR Code</h3>
		<p>
		Use the QR Code, below, in your print media. To save it as an image, right click and select "save image as".
		</p>	
	</div>
	<div id="qrcode"></div>
			<script type="text/javascript">
				new QRCode(document.getElementById("qrcode"), "'.$pagelink.'");
			</script>
	';
	
	if ( isset($atp_page) && !empty($atp_page) ){
		$output = $isapage;
	} else {
		$output = $nopageyet;
	}
	return $output;
}

// add the QR code js
// add jquery adder to admin
function atp_enqueue_qr_js() {
	global $current_screen;
	if ( $current_screen->id == 'toplevel_page_add-this-podcast' ) {
		$url = plugins_url( 'includes/js/davidshimjs-qrcodejs-1c78ccd/qrcode.js', dirname(__FILE__) );
		wp_enqueue_script("qr-code-js", $url );
	}
}
add_action('admin_enqueue_scripts', 'atp_enqueue_qr_js');

?>