# Custom Admin Pages for WordPress

Does the Settings API drive you crazy? You're not the only one! This is a library for wordpress to help you quickly and easily create new admin pages of any type. It was heavily inspired by Custom Metaboxes and Fields plugin.

## Changelog

### 0.4
* No bugs found after more extensive use. Declaring this version relatively stable.

### 0.3
* Changed prefix, added new page options.

### 0.1
* Initial commit. Extremely WIP.
