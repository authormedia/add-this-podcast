<?php
	add_action('admin_enqueue_scripts', 'atp_enqueue_admin_js');
	add_action('admin_menu', 'atp_add_sort_buttons_page',25);
//	add_action('admin_head', 'atp_remove_sort_buttons_page'); // use if you want to keep the page but not have the menu link.
	

function atp_enqueue_admin_js() {
	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script('jquery-ui-sortable');
}


function atp_add_sort_buttons_page() {
	add_submenu_page('add-this-podcast', 'Arrange Buttons', 'Arrange buttons', 'manage_options', 'atp-sort-buttons', 'atp_render_sort_buttons_page');
}


function atp_remove_sort_buttons_page() {
//	remove_submenu_page("atp_dashboard", "atp_sort_buttons");
}
	

function atp_render_sort_buttons_page() {
	global $wpdb, $post;
	if(!empty($_REQUEST['atp_button_order'])) {
		$data = json_decode(str_replace('\"', '"', $_REQUEST['atp_button_order']));
		if(!empty($data) and is_object($data)) {
			$data = (array)$data;
			$new_values = array();
			foreach($data as $key => $value) {
			//print_r('<pre style="padding: 10px; border: 1px solid #000; margin: 10px">'); print_r( $key .' - '. $value ); print_r('</pre>');
				$new_values[$key] = $value;
			}
		 update_option( 'atp-sort-podcasts', $new_values );
		}
			$sort = get_option('atp-sort-podcasts');
	}
?>
	<div class="wrap atp_sort_buttons">
		<div id="icon-options-general" class="icon32"><br></div><h2>Sort buttons</h2>
		<form id="atp_sort_buttons_form" method="post" action="<?php echo(admin_url('admin.php?page=atp-sort-buttons')); ?>">
			<p class="submit"><input type="submit" name="save_settings" id="submit" class="button button-primary" value="Save Changes" onclick="return atp_submit_button_order();"></p>
			<input id="atp_button_order" name="atp_button_order" type="hidden" value="">
			<?php
					$services = get_ordered_services();
					// order the services 
					echo('<ul id="atp_button_sorter">');
						foreach($services as $serv){
							$servname = $serv['name'];
							
							if($servname != 'itunes'){  // yeah itunes does not want its name to be capitalized "dare to be different"
								$servtitle = ucwords($servtitle);
							}
							$btn = $servname.'-sbutton.png';
							$imgurl = plugins_url( 'images/'.$btn,dirname(__FILE__) );
							$img = '<img class="big" src="'.$imgurl.'" />';
							
							$servtitle = str_replace('_',' ',$servname);
							
							$serv['smbutn'] = $url.$servname.'-sbutton.png';
							$link_obj = '<img class="small" src="'.$serv['smbutn'].'" />';
							echo '<li data-id="'.$serv['id'].'" class="atp_button '.$serv['name'].'">'.$img.'</li>';
						} // end foreach
					echo('</ul>');
			?>
		</form>
	</div>
	<script type="text/javascript">
		jQuery.fn.reverse = [].reverse;

		function atp_submit_button_order() {
			data = {};
			jQuery("#atp_button_sorter .atp_button").each(function(i, e) {
				console.log(e);	
				data[jQuery(e).attr('data-id')] = i;
				
			});
			jQuery("#atp_button_order").val(JSON.stringify(data));
			return true;
		}

		jQuery(document).ready(function() {
			jQuery("#atp_button_sorter").sortable();
		});
	</script>
<?php
} 